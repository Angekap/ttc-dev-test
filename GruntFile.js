(function() {
  module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),

      jshint: {
        files: ["*.js", "routes/*.js"],
        options: {
          esnext: true,
          globals: {
            jQuery: true
          }
        }
      },

      // for production build only
      preprocess: {
        options: {
          inline: true,
          context: {
            DEBUG: true
          }
        },
        js: { //dev
          src:['src_assets/js/*.js']
        }
      },

      copy: {
        "all": {
          files: [{
            expand: true,
            dest: 'public/assets',
            cwd: 'src_assets',
            src: '**',
            flatten: false
          }]
        }
      },

      less: {
        "dev": {
          options: {
            compressed: false
          },
          files: {
            "public/assets/css/test-private.css" : "less/test/includes-test.less"
          }
        }
      },

      uglify: {
        "prod": {
          options: {
            banner: "/*! Grunt minif */\n",
            compress: {
              drop_console: true
            }
          },
          files: [{
            extDot: 'last',
            src: [
              '*.js',
              '**/*.js'
            ],
            dest: 'public/assets/js/',
            cwd: 'src_assets/js/',
            ext: '.js',
            flatten: false,
            expand: true
          }]
        }
      },

      clean: {
        css: ['public/assets/css/test-private.css']
      },

      nodemon: {
        dev: {
          script: 'app.js',
          options: {
            args: ['dev'],
            // nodeArgs: ['--debug'],
            callback: function (nodemon) {
              nodemon.on('log', function (event) {
                console.log(event.colour);
              });
            },
            env: {
              PORT: '5000'
            },
            cwd: '**',
            ignore: ['node_modules/**', 'public/assets/daata/**'],
            ext: 'less,js,ejs,json',
            watch: ['server'],
            delay: 1000,
            legacyWatch: true
          }
        },
        exec: {
          options: {
            exec: 'less'
          }
        }
      },

      watch: {
        "dev": {
          files: ['less/**/*.less', 'routes/*.js', 'src_assets/js/*.js', '*.js', 'views/**/*.ejs'],
          tasks: ['clean:css', 'preprocess:js', 'less:dev'],
          options: {
            livereload: true,
            spawn: true
          }
        }
      }
    });

    grunt.registerTask("dev", ['clean:css', 'jshint', 'preprocess:js', 'copy:all', 'less:dev', 'watch:dev']);
  };
}).call(this);

import express from 'express';
import fs from 'fs';
import reload from 'reload';
import http from 'http';
import bodyParser from 'body-parser';
import fetch from 'node-fetch';
import routes from './routes/index';

const app = express();
var language;

routes(app);

// body parser
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

//Listening to available port from environment variable
//If no environment variable is set, use port 5000 as default
app.set('port', process.env.PORT || 5000);

/*
  Setup viewing engine - ejs (embedddable Javascript)
*/
app.set('view engine', 'ejs');
app.set('views', 'views'); //setup views location

/*
  Create a global variable that can is available to any of my routes (pages)
*/
app.locals.siteTitle = 'TTC Dev ';

/*
  Express middleware - express.static:
  method used to setup a folder for holding files that I want the use or routes to have access to.
  This is accessible through the root of my document.
*/
app.use(express.static('public/assets'));


app.post('/', function(req, resp) {

  language = req.body.language; //get user property

  if(language == '') {
    resp.json('Failed!');
  }

  if(language !== '') {
    const dataUrl = 'https://api.github.com/search/repositories?q=language:'+language+'&sort=stars&order=desc';
    fetch(dataUrl)
    .then(response => {
      response.json().then(json => {
        resp.json(json);
      });
    })
    .catch(error => {
      console.log(error);
    });
  }
});

const server = http.createServer(app);
server.listen(app.get('port'), function(){
  console.log('Listening on port ' + app.get('port'));
});

reload(app);

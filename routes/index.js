import express from 'express';

const routes = (app) => {
  app.route('/')
  .get((req, resp) => {

    resp.render('index', {
      // local variables that can be used in the views/pages
      // '<%= var name here %>' this is how to use defined variables in our pages (views)
      // data: data,

      pageTitle: ' Home',
      pageID: 'home-page',
    });
  });
};

export default routes;

# README #

This README documents necessary steps to get the application up and running.

### What is this repository for? ###

* Quick summary - a simple basic REST API
* Version - 1

### How do I get set up? ###

* Summary of set up - follow the steps below to get the application up and running
* Configuration - make sure you have both Node.js and NPM installed
* Clone this repo to a folder of your choice
* `cd` into the folder and run `npm install` to install the project dependencies
* Run `grunt dev` to generate the required assets
* Run `npm start` to start the express server and start the app
* visit http://localhost:5000/

### Who do I talk to? ###

* Repo owner - Ange L. Tshibenji